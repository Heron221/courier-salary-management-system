from django.urls import path
from .views import WeeklySalaryView


urlpatterns = [
    path('weekly_salaries/', WeeklySalaryView.as_view()),
]
