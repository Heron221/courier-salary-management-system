from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import date


class Courier(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Trip(models.Model):
    customer_type_options = (
        ('restaurant', 'Restaurant'),
        ('library', 'Library'),
        ('flower_shop', 'Flower_Shop'),
        ('super_market', 'Super_Market'),
    )

    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, related_name='trips')
    trip_distance = models.PositiveIntegerField()  # trip distance in meters
    costumer_type = models.CharField(max_length=20, choices=customer_type_options, default='restaurant')
    datetime = models.DateTimeField(auto_now=True)
    distance_from_starting_point = models.IntegerField()  # courier's distance from the starting point of the trip
    wage = models.PositiveIntegerField()  # trip wage in tomans

    def __str__(self):
        return f'{self.courier.name} at {self.datetime} for {self.wage}'


class WageChangeInfo(models.Model):
    courier = models.ForeignKey(Courier, on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now=True)
    amount = models.PositiveIntegerField()
    description = models.TextField()

    class Meta:
        abstract = True


class WageBonus(WageChangeInfo):

    def __str__(self):
        return f'{self.amount} bonus for {self.courier.name} at {self.datetime}'


class WagePenalty(WageChangeInfo):

    def __str__(self):
        return f'{self.amount} penalty for {self.courier.name} at {self.datetime}'


class DailySalary(models.Model):
    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, related_name='daily_salaries')
    date = models.DateField(auto_now=True)
    salary = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.salary} daily salary for {self.courier.name} at {self.date}'

    @receiver(post_save, sender=Trip)
    @transaction.atomic
    def new_trip(sender, instance, created, **kwargs):
        if created:
            daily_salary, is_created = DailySalary.objects.get_or_create(courier=instance.courier, date=date.today())
            daily_salary.salary += instance.wage
            daily_salary.save()

    @receiver(post_save, sender=WageBonus)
    @transaction.atomic
    def new_bonus(sender, instance, created, **kwargs):
        if created:
            daily_salary, is_created = DailySalary.objects.get_or_create(courier=instance.courier, date=date.today())
            daily_salary.salary += instance.amount
            daily_salary.save()

    @receiver(post_save, sender=WagePenalty)
    @transaction.atomic
    def new_penalty(sender, instance, created, **kwargs):
        if created:
            daily_salary, is_created = DailySalary.objects.get_or_create(courier=instance.courier, date=date.today())
            daily_salary.salary -= instance.amount
            daily_salary.save()


class WeeklySalary(models.Model):
    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, related_name='weekly_salaries')
    date_of_saturday = models.DateField()
    salary = models.IntegerField()

    def __str__(self):
        return f'{self.salary} weekly salary for {self.courier.name}'
