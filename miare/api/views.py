from datetime import datetime
from rest_framework.views import APIView
from .models import WeeklySalary
from .serializers import WeeklySalarySerializer
from rest_framework.response import Response
from rest_framework import status


class WeeklySalaryView(APIView):
    def get(self, request, format=None):
        from_date_string = request.data['from_date']
        from_date = datetime.strptime(from_date_string, "%Y-%m-%d").date()
        to_date_string = request.data['to_date']
        to_date = datetime.strptime(to_date_string, "%Y-%m-%d").date()
        weekly_salaries = WeeklySalary.objects.filter(date_of_saturday__range=[from_date, to_date])

        serializer = WeeklySalarySerializer(weekly_salaries)
        return Response(serializer.data, status=status.HTTP_200_OK)
