from celery import Celery
from celery.schedules import crontab
from .models import Courier, DailySalary, WeeklySalary
from django.db.models import Sum
from datetime import date, timedelta

app = Celery()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(hour=0, minute=1, day_of_week='saturday'),
        test.s(),
    )


@app.task
def test():
    start_date = date.today() - timedelta(days=7)
    end_date = date.today()
    weekly_salaries = []
    for courier in Courier.objects.all():
        salary = DailySalary.objects.filter(courier=courier, date__range=[start_date, end_date])\
                                    .aggregate(salary_sum=Sum('salary'))['salary_sum']
        weekly_salary = WeeklySalary(courier=courier, date_of_saturday=start_date, salary=salary)
        weekly_salaries.append(weekly_salary)

    WeeklySalary.objects.bulk_create(weekly_salaries)
