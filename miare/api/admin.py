from django.contrib import admin
from .models import Courier, Trip, WageBonus, WagePenalty, DailySalary, WeeklySalary


admin.site.register(Courier)
admin.site.register(Trip)
admin.site.register(WageBonus)
admin.site.register(WagePenalty)
admin.site.register(DailySalary)
admin.site.register(WeeklySalary)
