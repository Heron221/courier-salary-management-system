from django.test import TestCase
from .models import Courier, Trip, WageBonus, WagePenalty, DailySalary
from datetime import date


class DailySalaryTest(TestCase):
    def setUp(self):
        hossein = Courier.objects.create(name='Hossein')

        Trip.objects.create(courier=hossein, trip_distance=2000, costumer_type='restaurant',
                            distance_from_starting_point=100, wage=30000)
        Trip.objects.create(courier=hossein, trip_distance=6350, costumer_type='library',
                            distance_from_starting_point=50, wage=80000)
        Trip.objects.create(courier=hossein, trip_distance=4900, costumer_type='flower_shop',
                            distance_from_starting_point=2000, wage=65000)

        WageBonus.objects.create(courier=hossein, amount=50000, description='Eyde Yalda')

        WagePenalty.objects.create(courier=hossein, amount=20000, description='Takhir')

    def test_daily_salary(self):
        hossein = Courier.objects.get(name='Hossein')
        daily_salary = DailySalary.objects.get(courier=hossein, date=date.today())

        self.assertEqual(daily_salary.salary, 205000)

