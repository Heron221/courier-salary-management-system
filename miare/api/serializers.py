from .models import Courier, WeeklySalary
from rest_framework import serializers


class CourierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courier
        fields = ['id', 'name']


class WeeklySalarySerializer(serializers.ModelSerializer):
    courier = CourierSerializer(many=False)

    class Meta:
        model = WeeklySalary
        fields = ['id', 'courier', 'date_of_saturday', 'salary']
